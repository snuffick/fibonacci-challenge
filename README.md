# README #

This project consists of a script that provides a solution for a specific task. The script is written on python.

The script contains a class named Fibonacci, which implements two methods:

* Numbers(n)   - implements a generator yielding Fibonacci numbers up to nth (throwing a ValueError exception if n wasn't an integer or was less or equal to zero)
* GetNumber(n) - returns nth Fibonacci number (throwing a ValueError exception if n wasn't an integer or was less or equal to zero)

The script contains a function:

* BuzzFizz(n) - returns a string containing one of the follows:
    * "Buzz" when n is divisible by 3
    * "Fizz" when n is divisible by 5
    * "FizzBuzz" when n is divisible by 15
    * "BuzzFizz" when n is prime
    * the value n itself otherwise

The script contains a main routine to execute this script for testing (it waits for user to input a Fibonacci number index or "exit" to end the script, then it prints information related to the task).