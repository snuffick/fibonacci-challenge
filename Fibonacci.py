import math
import sys

class Fibonacci:
    def GetNumber(self, n):
        number = 0

        for number in self.Numbers(n):
            pass

        return number

    def Numbers(self, n):
        if type(n) is not int:
            raise ValueError("Fibonacci index should be integer")
        if n <= 0:
            raise ValueError("Fibonacci index should be greater than 0")

        number = [ 1, 1 ]

        for i in range(n):
            next_number = number[0]

            number[0]  = number[1]
            number[1] += next_number

            yield next_number

def BuzzFizz(n):
    if type(n) is not int:
        raise ValueError("Argument should be integer")

    string = ""

    if n % 5 == 0:
        string += "Fizz"

    if n % 3 == 0:
        string += "Buzz"

    if n == 1:
        string = n
    elif string == "":
        string = "BuzzFizz"

        for i in range(2, int(math.sqrt(n) + 1)):
            if n % i == 0:
                string = n
                break

    return string

if __name__ == "__main__":
    while True:
        try:
            number = input("Enter desired Fibonacci number (type \"exit\" to quit): ")

            if number == "exit":
                break

            number = int(number)

            f = Fibonacci()

            number = f.GetNumber(number)

            print("Fibonacci: {0} (\"{1}\")".format(BuzzFizz(number), number))
        except ValueError:
            print("Number format error")
        except:
            print("Input format error")